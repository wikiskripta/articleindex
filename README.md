# ArticleIndex

Mediawiki extension.

## Description

* Extension displays index of words in an article.
* Adding tag <aindex display="">indexed_word</aindex>.
    * _display_ is optional. Displays text in attribute instead of indexed word in the list bellow.
* Words in these tags are displayed in place of tag <articleindex/> in alphabet order grouped by first letters. 
* Clicking the word highlights his tagged occurences and moves page to the first occurance of the word.


## Installation

* Make sure you have MediaWiki 1.39+ installed.
* Download and place the extension to your _/extensions/_ folder.
* Add the following code to your LocalSettings.php: `wfLoadExtension( 'ArticleIndex' )`;

## Usage

* Slova, která mají být vyhledatelná, uzavřete prosím do tagu `<aindex>slovo</aindex>`.
* Na místo, kde si přejete zobrazit rejstřík, vložte tag `<articleindex/>`.
* Kliknete-li pak na slovo v rejstříku, otagované slovo se v textu zvýrazní.

### Popis funkce s atributem display

Pokud se má v rejstříku zobrazovat jiná varianta slova, je na to nový atribut "display". Udělá se to takto:
```
<aindex display="Špenát">Špenátu</aindex>
Příklad:
V textu budou rozmístěny tyto tagy
<aindex display="Špenát">Špenát</aindex>
<aindex display="Špenát">Špenátu</aindex>
<aindex display="Špenát">Špenátem</aindex>
<aindex display="Špenát">Špenáte</aindex>
```
V rejstříku dole bude jen slovo "Špenát". A po kliknutí se v textu zvýrazní slova
Špenát, Špenátu, Špenátem, Špenáte mezi kterými lze přecházet.

### Příklad použití

<aindex>Lorem</aindex> ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. <aindex>Duis</aindex> autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores <aindex>legere</aindex> me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per <aindex>seacula</aindex> quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.

<articleindex/>

## Internationalization
This extension is available in English and Czech language. For other languages, just edit files in /i18n/ folder.


## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2023 First Faculty of Medicine, Charles University
